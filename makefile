default:
	@echo "Compiling..."
	javac -sourcepath src src/*.java -d out/src
	cp -f src/*.png -d out/src
	jar cmf src/manifest.mf out/imgrate.jar -C out/src .

run: default
	java -jar out/imgrate.jar