# Image Rater

Leo Huang

Swing program that allows for rating of images and filtering by rating

Made with openjdk 12.0.2 2019-07-16
on Microsoft Windows [Version 10.0.17763.805]

-----SETUP-----

In the directory with the makefile, enter 'make' into the command line. You can then go to the /out folder and enter 'java -jar imgrate.jar' to launch.

Alternatively, you can enter 'make run' to compile and run in one command.

Do not change the locations of the .png files in neither the /src or /out/src directories. If you do, it will fail to compile/execute.

-----INSTRUCTIONS-----

Use the load button to select any number of images of your choice. Selecting a folder (even if it hold images) will not work.

Click on a loaded image to bring up a separate window with a larger view.

Click the stars beside an image to set a rating. Click the stars on the toolbar to set the filter (only images with an equal orhigher rating will show).

Your session will automatically save upon exit.