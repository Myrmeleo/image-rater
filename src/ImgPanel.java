import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;

//class that represents the images, which will be loaded from a system directory by the user
public class ImgPanel extends JPanel {

    static private double maxThumbnailWidth = 160;
    static private double maxThumbnailHeight = 120;
    static private double maxEnlargedWidth = 600;
    static private double maxEnlargedHeight = 450;

    private ImageIcon thumbnail;
    private ImageIcon largeView;
    private ImageIcon imgDisplayed;
    private String name;
    //absolute path
    private String imgPath;
    private String creationDate;
    private StarRating rateBar = new StarRating();
    private JButton bClearRating = new JButton();
    MouseListener ml;
    JLabel lImage;
    JLabel lName;
    JLabel lDate;

    //1st constructor
    //f is the file to convert into a ImgPanel, guaranteed to be either a PNG, JPG, or GIF
    public ImgPanel(File f) {
        try {
            //get image from the file
            Image tempImg = ImageIO.read(f);
            imgPath = f.getAbsolutePath();
            //scale the thumbnail image so that it will reach either maxWidth or maxHeight
            double widthTNRatio = maxThumbnailWidth / tempImg.getWidth(null);
            double heightTNRatio = maxThumbnailHeight / tempImg.getHeight(null);
            double TNRatioToUse = Math.min(widthTNRatio, heightTNRatio);
            thumbnail = new ImageIcon(tempImg.getScaledInstance((int) (tempImg.getWidth(null) * TNRatioToUse),
                    (int) (tempImg.getHeight(null) * TNRatioToUse), Image.SCALE_DEFAULT));
            //now do the same for the enlarged view
            double widthEVRatio = maxEnlargedWidth / tempImg.getWidth(null);
            double heightEVRatio = maxEnlargedHeight / tempImg.getHeight(null);
            double EVRatioToUse = Math.min(widthEVRatio, heightEVRatio);
            largeView = new ImageIcon(tempImg.getScaledInstance((int) (tempImg.getWidth(null) * EVRatioToUse),
                    (int) (tempImg.getHeight(null) * EVRatioToUse), Image.SCALE_DEFAULT));
            //find the creation date
            BasicFileAttributes bfa = Files.readAttributes(f.toPath(), BasicFileAttributes.class);
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            creationDate = df.format(bfa.creationTime().toMillis());
        } catch (IOException e) {System.err.println("Failed to read image from " + f.getAbsolutePath()); System.exit(1);}
        //get properties of file
        name = f.getName();
        try {
            Image trashIcon = ImageIO.read(getClass().getResourceAsStream("/clear.png"));
            bClearRating.setIcon(new ImageIcon(trashIcon));
        } catch (IOException e) {System.err.println("Failed to load image clear.png");}
        //make labels and add
        imgDisplayed = thumbnail;
        lImage = new JLabel(imgDisplayed);
        lImage.setPreferredSize(new Dimension((int)maxThumbnailWidth, (int)maxThumbnailHeight));
        lImage.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        lName = new JLabel(name);
        lDate = new JLabel(creationDate);
        bClearRating.setPreferredSize(new Dimension(80,40));
        add(lImage);
        add(lName);
        add(lDate);
        add(rateBar);
        add(bClearRating);
        setVisible(true);
        //when the image is clicked, go to new window
        //window will allow for individual viewing of images
        ml = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JFrame fPic = new JFrame();
                fPic.setResizable(false);
                fPic.setSize(800,600);
                ImgPanel clone = ImgPanel.this.clone();
                clone.switchImage();
                fPic.setContentPane(clone);
                fPic.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        ImgPanel.this.setRating(clone.getRating());
                    }
                });
                fPic.setVisible(true);
            }
        };
        lImage.addMouseListener(ml);
        //implement clear ratings button
        bClearRating.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                rateBar.reset();
            }
        });
    }

    //2nd constructor for when we are loading a previously selected bunch of images
    //f is the file, r is the rating to start
    public ImgPanel(File f, int r) {
        //call first constructor
        this(f);
        rateBar.setRating(r);
        rateBar.showRating();
    }

    //add function that expands image in new window when clicked
    public ImgPanel clone() {
        ImgPanel theClone = new ImgPanel(new File(imgPath));
        theClone.setRating(getRating());
        //prevent clone from opening a new window
        theClone.removeClickFunction();
        return theClone;
    }

    public void removeClickFunction() {
        lImage.removeMouseListener(ml);
    }

    public void switchImage() {
        if (imgDisplayed.equals(thumbnail)) {
            imgDisplayed = largeView;
        } else {
            imgDisplayed = thumbnail;
        }
        remove(lImage);
        lImage = new JLabel(imgDisplayed);
        add(lImage);
    }

    //in a grid layout, elements should be stacked, so use box layout
    public void setGridLayout() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }

    public void setListLayout() {
        setLayout(new FlowLayout());
    }

    public String getName() {return name;}

    public String getDate() {return creationDate;}

    public String getPath() {return imgPath;}

    public int getRating() {return rateBar.getRating();}

    public void setRating(int r) {
        rateBar.setRating(r);
    }
}