//Leo Huang
//App that allows rating and sorting of images

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

public class Main {

    //frame that holds everything
    private JFrame fMain = new JFrame("Leo's Awesome Fotag App");
    //panel that holds toolbar and image gallery
    private JPanel pMain = new JPanel();
    //panel that holds the images (ImgPanels)
    private JPanel pGallery = new JPanel();
    //make it scrollable
    private JScrollPane spGallery = new JScrollPane(pGallery, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    //toolbar that hold buttons
    private JToolBar tbarMain = new JToolBar();
    //button to load images
    private JButton bLoadImg = new JButton();
    //button to clear images
    private JButton bClearImg = new JButton();
    //buttons that switch layout
    private JToggleButton tbGridLayout = new JToggleButton();
    private JToggleButton tbListLayout = new JToggleButton();
    private ButtonGroup bgLayoutSwitch = new ButtonGroup();
    //filter by rating
    private StarRating srFilter = new StarRating();
    //button to reset filter
    private JButton bResetFilter = new JButton();
    //images to be displayed in the panel
    private ArrayList<ImgPanel> images = new ArrayList<>();

    //load image data when program is opened
    private void loadOnOpen() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("src/last-session-images.txt")));
            String filter = br.readLine();
            if (filter != null) {
                srFilter.setRating(Integer.parseInt(filter));
                String path;
                int rating;
                while ((path = br.readLine()) != null) {
                    rating = Integer.parseInt(br.readLine());
                    ImgPanel ft = new ImgPanel(new File(path), rating);
                    ft.setGridLayout();
                    images.add(ft);
                    pGallery.add(ft);
                }
                filterImages();
            }
        } catch (IOException e) {System.out.println("No previous session recorded.");}
    }

    //save image data (path and rating) when the program is closed
    private void saveOnClose() {
        try {
            PrintWriter writer = new PrintWriter(new File("src/last-session-images.txt"));
            writer.println(srFilter.getRating());
            for (ImgPanel ft: images) {
                writer.println(ft.getPath());
                writer.println(ft.getRating());
            }
            writer.close();
        } catch (IOException e) {System.err.println("Failed to save image data."); System.exit(1);}
    }

    //set up button appearance
    private void styleButtons() {
        //set icon to button that loads images
        try {
            Image folderIcon = ImageIO.read(getClass().getResourceAsStream("/folder.png"));
            bLoadImg.setIcon(new ImageIcon(folderIcon));
        } catch (Exception e) {System.err.println("Failed to load image folder.png");}
        //button that clears images
        try {
            Image clearIcon = ImageIO.read(getClass().getResourceAsStream("/clear.png"));
            bClearImg.setIcon(new ImageIcon(clearIcon));
            bResetFilter.setIcon(new ImageIcon(clearIcon));
        } catch (Exception e) {System.err.println("Failed to load image clear.png");}
        //layout buttons
        try {
            Image gridIcon = ImageIO.read(getClass().getResourceAsStream("/grid.png"));
            tbGridLayout.setIcon(new ImageIcon(gridIcon));
        } catch (Exception e) {System.err.println("Failed to load image grid.png");}
        try {
            Image listIcon = ImageIO.read(getClass().getResourceAsStream("/list.png"));
            tbListLayout.setIcon(new ImageIcon(listIcon));
        } catch (Exception e) {System.err.println("Failed to load image list.png");}
        //default to grid layout
        tbGridLayout.setSelected(true);
        //make it so that only one of the buttons can be selected at a time
        bgLayoutSwitch.add(tbGridLayout);
        bgLayoutSwitch.add(tbListLayout);
    }

    //set up button functions
    private void implButtons() {
        //load images when load button is pressed
        bLoadImg.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                chooseImg();
            }
        });
        //empty images when "clear" button is pressed
        bClearImg.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                for (Iterator<ImgPanel> i = images.iterator(); i.hasNext();) {
                    // remove the current element from the iterator and the list
                    pGallery.remove(i.next());
                    i.remove();
                }
                pGallery.revalidate();
                pGallery.repaint();
            }
        });
        //switch to grid layout
        tbGridLayout.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                pGallery.setLayout(new WrapLayout());
                for (ImgPanel ft: images) {
                    ft.setGridLayout();
                }
                pGallery.revalidate();
            }
        });
        //switch to list layout
        tbListLayout.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                pGallery.setLayout(new BoxLayout(pGallery, BoxLayout.Y_AXIS));
                for (ImgPanel ft: images) {
                    ft.setListLayout();
                }
                pGallery.revalidate();
            }
        });
        //filter using star bar
        srFilter.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                filterImages();
            }
        });
        //reset rating filter
        bResetFilter.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                srFilter.reset();
                for (ImgPanel ft: images) {
                    ft.setVisible(true);
                }
            }
        });
    }

    //filter images by rating according to star bar
    private void filterImages() {
        for (ImgPanel ft: images) {
            if (ft.getRating() < srFilter.getRating()) {
                ft.setVisible(false);
            } else {
                ft.setVisible(true);
            }
        }
    }

    //open a file chooser for user to select images
    private void chooseImg() {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "PNG", "JPG", "GIF", "png", "jpg", "gif");
        chooser.setFileFilter(filter);
        chooser.setMultiSelectionEnabled(true);
        chooser.showOpenDialog(fMain);
        for (File f: chooser.getSelectedFiles()) {
            ImgPanel ft = new ImgPanel(f);
            if (pGallery.getLayout() instanceof WrapLayout) {
                ft.setGridLayout();
            } else {
                ft.setListLayout();
            }
            images.add(images.size(), ft);
            pGallery.add(images.get(images.size() - 1));
        }
        pGallery.revalidate();
    }

    //initialize toolbar for app including all contents
    private void makeToolbar() {
        //set up toolbar on top of window
        tbarMain.setFloatable(false);
        tbarMain.setLayout(new FlowLayout());
        tbarMain.add(new JLabel("Images"));
        tbarMain.addSeparator();
        tbarMain.add(bLoadImg);
        tbarMain.add(bClearImg);
        tbarMain.addSeparator();
        tbarMain.add(new JLabel("Layouts"));
        tbarMain.addSeparator();
        tbarMain.add(tbGridLayout);
        tbarMain.add(tbListLayout);
        tbarMain.addSeparator();
        tbarMain.add(new JLabel("Filter"));
        tbarMain.addSeparator();
        tbarMain.add(srFilter);
        tbarMain.addSeparator();
        tbarMain.add(bResetFilter);
    }

    //initialize main frame and panel including all contents inside
    private void makeFrame() {
        fMain.setLayout(new BorderLayout());
        fMain.setSize(800,600);
        fMain.setMinimumSize(new Dimension(100,100));
        fMain.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        //image data should be saved when window closes
        fMain.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                saveOnClose();
            }
        });
        //add the main panel
        fMain.setContentPane(pMain);
        //add the toolbar and image panel to the main panel
        pMain.setLayout(new BorderLayout());
        pMain.add(tbarMain, BorderLayout.NORTH);
        pGallery.setLayout(new WrapLayout());
        pMain.add(spGallery, BorderLayout.CENTER);
        //ready
        fMain.setVisible(true);
    }

    public Main() {
        //load images from a previous session if they exist
        loadOnOpen();
        makeFrame();
        makeToolbar();
        styleButtons();
        implButtons();
    }

    public static void main(String[] args) {
        Main theApp = new Main();
    }
}
