import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

//represents the mechanism for rating images: a row of 5 stars
//stars will fill to the level the user clicks, and can be used to sort images by rating
public class StarRating extends JLabel {

    private int rating = 0;

    //1st constructor
    public StarRating() {
        setFont(new Font("Serif", Font.PLAIN, 36));
        //default to empty stars
        setText("\u2606\u2606\u2606\u2606\u2606");
        addMouseListener(new MouseAdapter() {
            @Override
            //determine which fifth of the label was clicked, and fill an appropriate number of stars
            public void mouseClicked(MouseEvent e) {
                int clickedX = e.getX();
                double starWidth = StarRating.this.getPreferredSize().getWidth() / 5;
                if (clickedX >= 0 && clickedX < starWidth) {                        //1 star
                    rating = 1;
                } else if (clickedX >= starWidth && clickedX < 2 * starWidth) {     //2 stars
                    rating = 2;
                } else if (clickedX >= 2 * starWidth && clickedX < 3 * starWidth) { //3 stars
                    rating = 3;
                } else if (clickedX >= 3 * starWidth && clickedX < 4 * starWidth) { //4 stars
                    rating = 4;
                } else {                                                            //5 stars
                    rating = 5;
                }
                showRating();
            }
        });
    }

    //set the stars in the text to reflect the current rating
    public void showRating() {
        if (rating == 1) {
            setText("\u2605\u2606\u2606\u2606\u2606");
        } else if (rating == 2) {
            setText("\u2605\u2605\u2606\u2606\u2606");
        } else if (rating == 3) {
            setText("\u2605\u2605\u2605\u2606\u2606");
        } else if (rating == 4) {
            setText("\u2605\u2605\u2605\u2605\u2606");
        } else if (rating == 5) {
            setText("\u2605\u2605\u2605\u2605\u2605");
        } else {
            setText("\u2606\u2606\u2606\u2606\u2606");
        }
    }

    public void reset() {rating = 0; setText("\u2606\u2606\u2606\u2606\u2606");}

    public int getRating() {return rating;}

    public void setRating(int r) {rating = r; showRating();}
}